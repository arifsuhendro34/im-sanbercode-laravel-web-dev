<?php

require_once('animal.php');
require_once('ape.php');
require_once('frog.php');

$sheep = new animal("shawn");
echo "Nama Hewan : " . $sheep->nama . "<br>";
echo "Jumlah Kaki : " . $sheep->legs . "<br>";
echo "Berdarah dingin : " . $sheep->cold_blooded . "<br><br>";

$kodok = new frog("buduk");
echo "Nama Hewan : " . $kodok->nama . "<br>";
echo "Jumlah Kaki : " . $kodok->legs . "<br>";
echo "Cold Blooded : " . $kodok->cold_blooded . "<br>";
echo "Jump : " . $kodok->jump . "<br><br>";

$sungokong = new ape("kera sakti");
echo "Nama Hewan : " . $sungokong->nama . "<br>";
echo "Jumlah Kaki : " . $sungokong->legs . "<br>";
echo "Cold Blooded : " . $sungokong->cold_blooded . "<br>";
echo "Yell : " . $sungokong->yell . "<br><br>";


?>