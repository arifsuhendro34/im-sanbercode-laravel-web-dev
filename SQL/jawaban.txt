1. Buat Database
create DATABASE myshop

2 Buat Tabel dalam Database
users
CREATE TABLE users( id int(15) AUTO_INCREMENT PRIMARY KEY, name varchar(255) NOT null, email varchar (255) NOT null, password varchar (255) NOT null )

categories
CREATE TABLE categories ( id int(15) AUTO_INCREMENT PRIMARY KEY, name varchar (255) NOT null )

items
CREATE TABLE items ( id int(15) AUTO_INCREMENT PRIMARY KEY, name varchar (255) NOT null, description varchar (255) NOT null, price int (15) NOT null, stock int (15) NOT null, category_id int (15) NOT null, FOREIGN KEY(category_id) REFERENCES categories(id) )

3. Masukkan data pada tabel
users
INSERT INTO users (name,email,password) VALUES ("John Doe", "john@gmail.com","john123"), ("Jane Doe", "jane@gmail.com","jenita123")

categories
INSERT INTO categories(name) VALUE ("gadget"), ("cloth"), ("men"), ("women"), ("branded")

items
INSERT INTO items (name, description, price, stock, category_id) VALUES ("Sumsang b50", "hape keren dari merk sumsang", "4000000", "100", "1")
INSERT INTO items (name, description, price, stock, category_id) VALUES ("Uniklooh", "baju keren dari brand ternama", "500000", "50", "2")
INSERT INTO items (name, description, price, stock, category_id) VALUES ("IMHO watch", "jam tangan anak yang jujur banget", "2000000", "10", "1")

4. Mengambil data dari database
a. mengambil data users
SELECT id, name, email FROM users

b.mengambil data items
-SELECT * FROM items WHERE price > 1000000 
-SELECT * FROM items WHERE name LIKE "%watch" 

c.menampilkan data items join dengan kategori
SELECT items.* , categories.name as kategori from items INNER JOIN categories ON items.category_id = categories.id 

5.ubah data dari database
UPDATE items SET price = 2500000 WHERE id = 1 